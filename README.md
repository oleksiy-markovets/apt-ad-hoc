# apt-ad-hoc

Create your own ad hoc APT repo encapsulated into docker container

## How To

* Initiate image with empty APT repo

	```bash
	git clone git@gitlab.com:/oleksiy-markovets/apt-ad-hoc.git 
	cd apt-ad-hoc
	docker build . -t registry-host:5000/my-project-apt
	```

* Push image to docker registry 
  ```bash
  docker push registry-host:5000/my-project-apt
  ```

* Add packages to image

	* Created Dockerfile to add images like this

		```dockerfile
		FROM registry-host:5000/my-project-apt:latest as build

		ADD ./debbuild /root/debuild
		RUN reprepro include unstable ./debuild/*.changes

		FROM registry-host:5000/my-project-apt:latest 
		COPY --from=build /var/www/html/ /var/www/html/
		```

	* Build new image and push it to docker registry

		```bash
		dpkg-buildpackage -b -uc
		docker build ../ -f path/to/Dockerfile -t registry-host:5000/my-project-apt:latest
		docker push registry-host:5000/my-project-apt:latest
		```

* Use image to install packages on host

	* Run container
		```bash
		docker run registry-host:5000/my-project-apt:latest -p 80:80
		```

	* Add archive key

		```bash
		wget http://localhost/archive-key.asc -O - | sudo apt-key add -
		```

	* Add archive to sourcelist 

		```bash
		sudo apt-add-repository -u 'deb http://localhost:/debian unstable main'
		```

	* Install packages

		```bash
		sudo aptitude install my-project
		```

* Use image to install packages in gitlab-ci job

	* Create gitlab ci job like this 
		```yaml
		test:
		  image: debian:testing
		  services:
		    - name: registry-host:5000/my-project-apt:latest
		      alias: my-project-apt
		  variables:
		    DEBIAN_FRONTEND: noninteractive
		  before_script:
		    - apt-get update
		    - apt-get install -y wget gpg software-properties-common 
		    - wget http://my-project-apt/archive-key.asc -O - | apt-key add - 
		    - apt-add-repository -u 'deb http://my-project-apt:/debian unstable main'
		    - apt-get install -y my-project
		  before_script:
		    - make test
		```
* Use image to install package while building docker image with gitlab-ci

	* Create gitlab ci job like this 
		```yaml
		build:
		  image: docker:latest
		  services:
		    - name: registry-host:5000/my-project-apt:latest
		      alias: my-project-apt
		    - docker:dind
		  before_script:
		    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
		  script:
		    - export MY_PROJECT_APT=$(cat /etc/hosts | grep my-project-apt | head -n 1 | awk '{print $1}')
		    - docker build --add-host="my-project-apt:${MY_PROJECT_APT}" .
		    - docker push "${CI_REGISTRY}/${CI_PROJECT_PATH}/build:0.0.2"
		```
	* Create Dockerfile like this 
		```dockerfile
		FROM debian:testing
		
		ENV DEBIAN_FRONTEND="noninteractive"
		
		RUN apt-get update && \
			apt-get install -y wget gpg software-properties-common && \
			wget http://my-project-apt/archive-key.asc -O - | apt-key add - && \
			apt-add-repository -u 'deb http://my-project-apt:/debian unstable main' && \
			apt-get install -y my-project && \
			apt-add-repository --remove 'deb http://my-project-apt:/debian unstable main' && \
			rm /etc/apt/trusted.gpg && \
			apt-get purge -y wget gpg software-properties-common && \
			apt-get clean
		```
