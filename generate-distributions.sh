#!/bin/bash
cat >> /var/www/html/debian/conf/distributions <<REM 
Origin: APT ad hoc
Label: APT ad hoc
Codename: unstable 
Architectures: amd64 
Components: main source
Description: Apt ad hoc reposictory 
SignWith: $(gpg --list-keys --with-colons reprepro | grep pub | sed 's/^pub:u:[0-9]*:[0-9]*:\([0-9A-F]*\):.*/\1/g')
REM

