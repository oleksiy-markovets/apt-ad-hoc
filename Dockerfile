FROM debian:testing

EXPOSE 80

ENV DEBIAN_FRONTEND=noninteractive
ENV REPREPRO_BASE_DIR=/var/www/html/debian

RUN apt-get update && \
	apt-get install -y reprepro gpg apache2

WORKDIR /root/
ADD gpg-key.conf /root/
ADD generate-distributions.sh /root/
ADD apache.conf /etc/apache2/conf.d/repos

RUN gpg --batch --generate-key gpg-key.conf && \
	gpg --armor --output /var/www/html/archive-key.asc --export reprepro && \
	mkdir -p /var/www/html/debian/conf && \
	bash ./generate-distributions.sh

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
